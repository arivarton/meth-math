import sympy as sym
import numpy as np

class Integral(object):
    def __init__(self, function, a=0, b=3, n=100) -> None:
        self.function(function)
        self.a = self._parse_math(a)
        self.b = self._parse_math(b)
        self.n = n
        self.round = 4
        self.ee_constant = None
        self.printing = True
        self._dx(n)

    def _dx(self, n) -> float:
        self.dx = (self.b-self.a)/n
        return self.dx

    def _parse_math(self, m) -> float:
        return sym.parse_expr(str(m)).evalf()

    def _print_number(self, s) -> None:
        if self.printing:
            print(round(s, self.round))

    def function(self, f) -> None:
        self.f = sym.lambdify(sym.Symbol('x'), f)

    def limits(self, a=None, b=None) -> None:
        self.a = self._parse_math(a or self.a)
        self.b = self._parse_math(b or self.b)
        self._dx(self.n)

    def iterations(self, n) -> None:
        self.n = n
        self._dx(self.n)

    def midpoint(self) -> float:
        summ = 0
        for i in range(1, self.n+1):
            x_i = self.a+(i-1/2)*self.dx
            summ += self.f(float(x_i))
        result = summ * self.dx
        self.set_ee_constant('midpoint')
        self._print_number(result) 
        return result

    def trapes(self) -> float:
        summ = 0
        for i in range(0, self.n+1):
            x_i = self.a+i*self.dx
            if i == 0 or i == self.n:
                summ += self.f(float(x_i))
            else:
                summ += self.f(float(x_i))*2
        result = summ * (self.dx/2)
        self.set_ee_constant('trapes')
        self._print_number(result) 
        return result

    def simpsons(self) -> float:
        summ = 0
        dx = self._dx(self.n)
        for i in range(0, self.n+1):
            x_i = self.a+i*dx
            if i == 0 or i == self.n:
                summ += self.f(float(x_i))
            else:
                if i%2:
                    summ += self.f(float(x_i))*4
                else:
                    summ += self.f(float(x_i))*2
        result = summ * (dx/3)
        self.set_ee_constant('simpsons')
        self._print_number(result) 
        return result

    def set_ee_constant(self, t):
        if t == 'midpoint':
            self.ee_constant = (24, 2, 2)
        elif t == 'trapes':
            self.ee_constant = (12, 2, 2)
        elif t == 'simpsons':
            self.ee_constant = (180, 5, 4)

    def error_estimation(self, M) -> float:
        M = self._parse_math(M)
        result = (M*(self.b-self.a)**self.ee_constant[1])/(self.ee_constant[0]*self.n**self.ee_constant[2])
        self._print_number(result)
        return result

    def min_error_iterations(self, M, limit, start=1):
        self.printing = False
        self.n = start
        while integral.error_estimation(M) > limit:
            self.n += 1
        self._dx(self.n)
        self.printing = True
        self._print_number(self.n)
        return self.n


integral = Integral('cos(x)', b='pi', n=75)
integral.midpoint()
integral.trapes()

integral.function('cos(x^2)')
integral.midpoint()
integral.trapes()

# Buelengde av exp(-x) fra 0 til 1
integral.function('sqrt(1+exp(-2*x))')
integral.limits(b=1)
integral.iterations(5)
integral.trapes()
integral.error_estimation('3/4*sqrt(2)')

# Integral av ln(x^2 + 1)
integral.function('ln(x^2+1)')
integral.iterations(6)
integral.trapes()

# Feilestimering av e^(x^2) fra 0 til 1
integral.function('e^(x^2)')
integral.set_ee_constant('trapes')
integral.min_error_iterations('6*exp(1)', 0.00001)
integral.trapes()

integral.function('0.2*x+0.4')
integral.limits(a=0, b=15)
integral.iterations(46)
integral.trapes()

# Simpsons metode
integral.function('2*pi*((x-x**2)/cos(x))')
integral.limits(a=0, b=1)
integral.iterations(4)
integral.set_ee_constant('simpsons')
integral.simpsons()
