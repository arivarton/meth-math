from sys import argv
import argparse

import sympy as sym


class Derivative(object):
    def __init__(self, function, times):
        self.f = function
        self.times = times
        self.derivative = self.derive()
        self.calculate = sym.lambdify(sym.Symbol('x'), self.derivative)

    def __str__(self):
        return sym.pretty(self.derivative)

    def derive(self):
        d = self.f
        for i in range(self.times):
            d = sym.diff(d, sym.Symbol('x'))
        return d

def args():
    # Command line parser
    parser = argparse.ArgumentParser("Derivative")
    parser.add_argument("function", help="Function to be derived.", default='x^2', type=str)
    parser.add_argument("--times", help="How many times to derive.", default=1, type=int)
    return parser

if __name__ == '__main__':
    args = args().parse_args()
    derivative = Derivative(args.function, args.times)
    print(derivative)
