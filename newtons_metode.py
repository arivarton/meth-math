import argparse
from sys import argv

import sympy as sym

class NewtonsMetode(object):
    """
    Only one variable named x currently supported.
    """
    def __init__(self, f='x^2', x0=1):
        self.f = sym.lambdify(sym.Symbol('x'), f)
        self.f_d = sym.lambdify(sym.Symbol('x'), self.derivative(f))
        self.f_dd = sym.lambdify(sym.Symbol('x'), self.derivative(self.derivative(f)))
        self.x0 = float(x0)
        self.x_points = []

    def derivative(self, f):
        return str(sym.diff(f, sym.Symbol('x')))

    def nullpunkt(self, x):
        if self.f_d(x) == 0:
            raise Exception('Null i nevner!')
        point = x - (self.f(x)/self.f_d(x))
        if self.x_points.count(point) > 9:
            raise Exception(f'Uendelig loop! {point} fantes allerede!')
        self.x_points.append(point)
        return point

    def extremum(self, x):
        if self.f_dd(x) == 0:
            raise Exception('Null i nevner!')
        point = x - (self.f_d(x)/self.f_dd(x))
        if self.x_points.count(point) > 9:
            raise Exception(f'Uendelig loop! {point} fantes allerede!')
        self.x_points.append(point)
        return point

    def finn_nullpunkt(self):
        xn = self.nullpunkt(self.x0)
        while xn != self.nullpunkt(xn):
            xn = self.nullpunkt(xn)
        self.xn = xn
        self.equation = f'{xn} - ({self.f_d(xn)}/{self.f_dd(xn)})'
        return xn

    def finn_extremum(self):
        xn = self.extremum(self.x0)
        while xn != self.extremum(xn):
            xn = self.extremum(xn)
        self.xn = xn
        self.equation = f'{xn} - ({self.f_d(xn)}/{self.f_dd(xn)})'
        return xn


class Parser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super(argparse.ArgumentParser, self).__init__(self, 'Newtons Method', *args, **kwargs)
        self.function_minus = False
        if argv[1].startswith('-'):
            self.function_minus = True
            argv[1] = argv[1][1:]
        self.prog = "Newtons Method"
        self.add_argument("function", help="Function to be derived.",
                default='x^2', type=str)
        self.add_argument("start", help="Start number.", default=1, nargs='?', type=int)
        self.add_argument("point",
                            default='nullpunkt',
                            const='nullpunkt',
                            nargs='?',
                            choices=['nullpunkt', 'extremum'],
                            help="Point to find.", type=str)

    def parse_args(self):
        super(self).parse_args(self)
        if self.function_minus:
            self.args.function = '-' + self.args.function
        return self.args 


if __name__ == '__main__':
    parser = Parser()
    args = parser.parse_args()
    newtons_metode = NewtonsMetode(args.function, args.start)
    if args.point == 'extremum':
        extremum = newtons_metode.finn_extremum()
        print('Topp eller bunnpunkt:', extremum)
        print('Siste likning:', newtons_metode.equation)
    elif args.point == 'nullpunkt':
        nullpunkt = newtons_metode.finn_nullpunkt()
        print('Nullpunkt:', nullpunkt)
        print('Siste likning:', newtons_metode.equation)
