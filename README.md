## METH-MATH

### Usage:

### Newtons metode
#### Extremum:
```bash
python newtons_metode.py '5*x**2-14*x+12' 3 extremum
```

#### Nullpunkt:
```bash
python newtons_metode.py '(x+sqrt(3))*(2-x^3)' 0
```

### Derivering
#### En gang:
```bash
python derivering.py 'x^3/2'
```
#### Flere ganger:
```bash
python derivering.py 'ln(x^3/2)' 7
```
