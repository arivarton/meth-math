import numpy as np
import matplotlib.pyplot as plt
import pandas


# Sett D=0 hvis ikke luftmotstand skal tas med i betraktning
class Posisjoner(object):
    def __init__(self):
        self.graphs = {}
        self.data = pandas.DataFrame(columns=['navn', 'x_pos', 'y_pos', 'steg', 'maks_hoyde','total_lengde','vinkel'])

    def akselerasjon(self, D, x, y):
        self.ax = (-self.D*x**2)*self.steg
        self.ay = (-9.81-self.D*y**2)*self.steg
        
    def nytt_punkt(self):
        # oppdaterer akselerasjon
        if self.D != 0:
            self.akselerasjon(self.D, self.vx, self.vy)
        # oppdaterer hastigheten
        self.vx = self.vx + self.ax
        self.vy = self.vy + self.ay
        self.xPos.append(self.xPos[self.i]+self.vx*self.steg)
        self.yPos.append(self.yPos[self.i]+self.vy*self.steg)
        self.i += 1
        
    def add_graph(self, name, initialhastighet, th0, D=0, start_x=0, start_y=0, y_ground=0, steg=0.001):
        self.initialhastighet = initialhastighet
        self.th0 = th0*np.pi/180
        self.vx = initialhastighet*np.cos(self.th0)
        self.vy = initialhastighet*np.sin(self.th0)
        self.D = D
        self.steg = steg
        self.y_ground = y_ground
        self.xPos = [start_x, start_x+self.vx*self.steg]
        self.yPos = [start_y, start_y+self.vy*self.steg]
        self.i = 1
        # Sett akselerasjon for f�rste punkt
        self.akselerasjon(D, start_x, start_y)
        # Sett f�rste punkt
        self.nytt_punkt()

        while not(self.yPos[self.i-1] > self.yPos[self.i] and self.yPos[self.i] <= self.y_ground):
            self.nytt_punkt()

        self.graphs.update({name: {'x': self.xPos, 'y': self.yPos, 'steg': self.steg,
                                   'maks_h': max(self.yPos), 'total_l': self.xPos[-1],
                                   'vinkel': th0}})
        self.data.loc[len(self.data)] = [name, self.xPos, self.yPos, self.steg, round(max(self.yPos), 2), round(self.xPos[-1], 2), th0]
            
    def show_graphs(self, legend=True):
        maks_hoyde = self.data.maks_hoyde.max()
        total_lengde = self.data.total_lengde.max()
        for i,data in self.data.iterrows():
            tid = round((len(data.y_pos)*data.steg), 2)
            if data.total_lengde == total_lengde or data.maks_hoyde == maks_hoyde:
                graph_color = 'red'
                line_style = '-'
            else:
                graph_color = 'blue'
                line_style = ':'
            # Plotter
            plt.plot(data.x_pos,data.y_pos, linestyle=line_style, color=graph_color, label=data.navn+f'\ntid: {tid}s\nlengde: {round(data.x_pos[-1], 2)}m')
            plt.ylabel("y [m]")
            plt.xlabel("x [m]")
        plt.grid()
        if legend:
            plt.legend()
    
    def show_critical_data(self, export=False, name='kulekast'):
        h = self.data[self.data.maks_hoyde == self.data.maks_hoyde.max()].filter(['maks_hoyde',
                                                                                  'total_lengde','vinkel'])
        print('\nHoyeste prosjektil:')
        print(h.to_markdown())
        
        l = self.data[self.data.total_lengde == self.data.total_lengde.max()].filter(['maks_hoyde',
                                                                                      'total_lengde','vinkel'])
        print('\nLengste prosjektil:')
        print(l.to_markdown())
        
        if export:
            self.data.filter(['maks_hoyde','total_lengde','vinkel']).to_latex(f'/home/vidr/maskin/{name}.txt')
        

# x = Posisjoner()
# x.add_graph(f'45 grader', 73, 45, 0)
# x.show_graphs()

x = Posisjoner()
for i in range(1,91):
    # Med luftmotstand
    x.add_graph(f'{i} grader', 73, i, 0)
    # Uten luftmotstand
    #x.add_graph(f'{i} grader', 73, 60)
x.show_graphs(legend=False)
x.show_critical_data(export=False)