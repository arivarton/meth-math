import argparse

from derivering import args as derivering_args
from derivering import Derivative
from newtons_metode import args as newton_method_args
from newtons_metode import NewtonsMetode


# Main parser
main_parser = argparse.ArgumentParser(description='Methmath')

# Sub parsers
main_subparsers = main_parser.add_subparsers()

# Derivative
def derive(args):
    print('jaja')
    derivative = Derivative(args.function, args.times)
    print('jau')
    print(derivative)

derive_parser = main_subparsers.add_parser('derive', aliases=['d'],
        add_help=False, parents=[derivering_args()])
derive_parser.set_defaults(func=derive)

# Newtons method
def newton_method(args):
    newtons_metode = NewtonsMetode(args.function, args.start)
    if len(argv) >= 4:
        if argv[3].lower() == 'extremum':
            extremum = newtons_metode.finn_extremum()
            print('Topp eller bunnpunkt:', extremum)
            print('Siste likning:', newtons_metode.equation)
    else:
        nullpunkt = newtons_metode.finn_nullpunkt()
        print('Nullpunkt:', nullpunkt)
        print('Siste likning:', newtons_metode.equation)

newton_method_parser = main_subparsers.add_parser('newtons-method', aliases=['n'],
        add_help=False, parents=[newton_method_args()])
newton_method_parser.set_defaults(func=newton_method)

main_parser.parse_args()
